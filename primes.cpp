/*
 Copyright 2024
 Author: Radek Zlebcik
*/

#include <iostream>

using std::cout;
using std::endl;

//////////////////////////////////////////
/// Fuction to check if a number is prime
//////////////////////////////////////////
bool isPrime(int n) {
  if (n <= 1)
    return false;
  for (int i = 2; i * i <= n; i++)
    if (n % i == 0)
      return false;
  return true;
}


int main() {
  cout << "Primes less than 100:" << endl;
  for (int i = 0; i < 100; i++) {
    if (isPrime(i))
      cout << i << endl;
  }

  return 0;
}
